import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Subscription} from 'rxjs';

import {CategoriesService} from '../../shared/services/categories.service';
import {Category} from '../../shared/models/category.model';
import {Message} from '../../../shared/models/message.model';

@Component({
  selector: 'wfm-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnInit, OnDestroy {

  sub1: Subscription;
  @Output() onCategoryAdd = new EventEmitter<Category>();
  message: Message;

  constructor(private categoriesService: CategoriesService) { }

  ngOnInit() {
    this.message = new Message('success', '');
  }

  ngOnDestroy() {
    if (this.sub1) { this.sub1.unsubscribe(); }
  }

  onSubmit(form: NgForm) {
    let { name, capacity } = form.value;
    const category = new Category(name, capacity);

    if (capacity < 0) {
      capacity *= -1;
    }

    this.sub1 = this.categoriesService.addCategory(category)
      .subscribe((c: Category) => {
        form.reset();
        form.form.patchValue({capacity: 1});
        this.onCategoryAdd.emit(c);
        this.message.text = 'Категория успешно добавлена!';
        setTimeout(() => this.message.text = '', 5000);
      });
  }

}
