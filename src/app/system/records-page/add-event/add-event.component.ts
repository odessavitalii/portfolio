import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import * as moment from 'moment';
import {mergeMap} from 'rxjs/operators';
import {Subscription} from 'rxjs';

import {Category} from '../../shared/models/category.model';
import {EventModel} from '../../shared/models/event.model';
import {Bill} from '../../shared/models/bill.model';
import {Message} from '../../../shared/models/message.model';
import {EventsService} from '../../shared/services/events.service';
import {BillService} from '../../shared/services/bill.service';

@Component({
  selector: 'wfm-add-event',
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.scss']
})
export class AddEventComponent implements OnInit, OnDestroy {

  sub1: Subscription;
  sub2: Subscription;

  @Input() categories: Category[] = [];
  types = [
    {
      type: 'income',
      label: 'Доход'
    },
    {
      type: 'outcome',
      label: 'Расход'
    }
  ];
  message: Message;

  constructor(
    private eventsService: EventsService,
    private billService: BillService
  ) { }

  ngOnInit() {
    this.message = new Message('danger', '');
  }

  ngOnDestroy() {
    if (this.sub1) { this.sub1.unsubscribe(); }
    if (this.sub2) { this.sub2.unsubscribe(); }
  }

  showMessage(text: string) {
    this.message.text = text;

    window.setTimeout(() => this.message.text = '', 5000);
  }

  onSubmit(form: NgForm) {
    const {type, amount, category, description} = form.value;

    if (amount < 0) {
      // @ts-ignore
      amount *= -1;
    }

    const date = moment().format('DD.MM.YYYY HH:mm:ss');
    const event = new EventModel(type, amount, +category, date, description);

    this.sub1 = this.billService.getBill().subscribe((bill: Bill) => {
      let value = 0;

      if (type === 'outcome') {

        if (amount > bill.billValue) {
          this.showMessage(`На счету недостаточно средств. Вам не хватает ${amount - bill.billValue}`);

          return;
        } else {
          value = bill.billValue - amount;
        }

      } else {
        value = bill.billValue + amount;
      }

      this.sub2 = this.billService.updateBill({billValue: value, billCurrency: bill.billCurrency})
        .pipe(mergeMap(() => this.eventsService.addEvent(event)))
        .subscribe(() => {
          form.setValue({
            type: 'outcome',
            amount: 0,
            category: 1,
            description: ''
          });
        });
    });
  }

}
