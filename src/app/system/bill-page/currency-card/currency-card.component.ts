import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'wfm-currency-card',
  templateUrl: './currency-card.component.html',
  styleUrls: ['./currency-card.component.scss']
})
export class CurrencyCardComponent implements OnInit {

  @Input() currencyFull: any;
  currencies: string[] = ['USD', 'EUR'];
  currency: any;
  date: Date;

  constructor() { }

  ngOnInit() {
    this.date = this.currencyFull[1].Date;
    this.currency = this.currencyFull[1]['Valute'];
  }

}
