import {Component, Input, OnInit} from '@angular/core';

import {Bill} from '../../shared/models/bill.model';

@Component({
  selector: 'wfm-bill-card',
  templateUrl: './bill-card.component.html',
  styleUrls: ['./bill-card.component.scss']
})
export class BillCardComponent implements OnInit {

  @Input() bill: Bill;
  @Input() currency: any;

  rub: number;
  dollar: number;
  euro: number;

  constructor() { }

  ngOnInit() {
    this.rub = this.bill.billValue;
    this.dollar = this.bill.billValue / this.currency['USD'].Value;
    this.euro = this.bill.billValue / this.currency['EUR'].Value;
  }

}
