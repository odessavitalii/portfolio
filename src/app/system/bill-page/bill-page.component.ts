import {Component, OnDestroy, OnInit} from '@angular/core';
import {combineLatest, Subscription} from 'rxjs';

import {Bill} from '../shared/models/bill.model';
import {BillService} from '../shared/services/bill.service';


@Component({
  selector: 'wfm-bill-page',
  templateUrl: './bill-page.component.html',
  styleUrls: ['./bill-page.component.scss']
})
export class BillPageComponent implements OnInit, OnDestroy {

  sub1: Subscription;
  sub2: Subscription;
  currency: {};
  currencyFull: any;
  bill: Bill;
  isLoaded = false;

  constructor(private billService: BillService) {
  }

  ngOnInit() {
    this.sub1 = combineLatest([
      this.billService.getBill(),
      this.billService.getCurrency()]
    ).subscribe((data: [Bill, {}]) => {
      this.bill = data[0];
      this.currency = data.map(c => c['Valute'])[1];
      this.currencyFull = data;
      this.isLoaded = true;
    });
  }

  ngOnDestroy() {
    if (this.sub1) {
      this.sub1.unsubscribe();
    }
    if (this.sub2) {
      this.sub2.unsubscribe();
    }
  }

  onRefresh() {
    this.isLoaded = false;
    this.sub2 = this.billService.getCurrency()
      .subscribe((currency) => {
        this.currency = currency.map(c => c['Valute']);
        this.currencyFull = currency;
        this.isLoaded = true;
      });
  }

}
