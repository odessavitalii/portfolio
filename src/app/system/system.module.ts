import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {NgxChartsModule} from '@swimlane/ngx-charts';

import {DropdownDirective} from './shared/directive/dropdown.directive';

import {MomentPipe} from './shared/pipe/moment.pipe';
import {FilterPipe} from './shared/pipe/filter.pipe';

import {SharedModule} from '../shared/shared.module';
import {SYSTEM_ROUTES} from './system-routing.module';

import {BillService} from './shared/services/bill.service';
import {CategoriesService} from './shared/services/categories.service';
import {EventsService} from './shared/services/events.service';

import {SystemComponent} from './system.component';
import {BillPageComponent} from './bill-page/bill-page.component';
import {HistoryPageComponent} from './history-page/history-page.component';
import {PlanningPageComponent} from './planning-page/planning-page.component';
import {RecordsPageComponent} from './records-page/records-page.component';
import {SidebarComponent} from './shared/components/sidebar/sidebar.component';
import {HeaderComponent} from './shared/components/header/header.component';
import {BillCardComponent} from './bill-page/bill-card/bill-card.component';
import {CurrencyCardComponent} from './bill-page/currency-card/currency-card.component';
import {EditCategoryComponent} from './records-page/edit-category/edit-category.component';
import {AddCategoryComponent} from './records-page/add-category/add-category.component';
import {AddEventComponent} from './records-page/add-event/add-event.component';
import {HistoryChartComponent} from './history-page/history-chart/history-chart.component';
import {HistoryDetailComponent} from './history-page/history-detail/history-detail.component';
import {HistoryEventsComponent} from './history-page/history-events/history-events.component';
import {HistoryFilterComponent} from './history-page/history-filter/history-filter.component';

@NgModule({
  declarations: [
    SystemComponent,
    BillPageComponent,
    HistoryPageComponent,
    PlanningPageComponent,
    RecordsPageComponent,
    SidebarComponent,
    HeaderComponent,
    DropdownDirective,
    BillCardComponent,
    CurrencyCardComponent,
    MomentPipe,
    FilterPipe,
    EditCategoryComponent,
    AddCategoryComponent,
    AddEventComponent,
    HistoryChartComponent,
    HistoryDetailComponent,
    HistoryEventsComponent,
    HistoryFilterComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(SYSTEM_ROUTES),
    NgxChartsModule
  ],
  exports: [],
  providers: [
    BillService,
    CategoriesService,
    EventsService
  ]
})

export class SystemModule { }
