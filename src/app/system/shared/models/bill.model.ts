export class Bill {
  constructor(
    public billValue: number,
    public billCurrency: string
  ) { }
}
