import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';

import {Category} from '../models/category.model';
import {BaseApi} from '../../../shared/services/base-api.service';
import {Observable} from 'rxjs';

@Injectable()
export class CategoriesService extends BaseApi {
  constructor(public http: HttpClient) {
    super(http);
  }

  addCategory(category: Category): Observable<Category> {
    return this.post('categories', category);
  }

  getCategories(): Observable<Category[]> {
    return this.get('categories');
  }

  updateCategory(category: Category): Observable<Category> {
    return this.put(`categories/${category.id}`, category);
  }

  getCategoryById(id: number): Observable<Category> {
    return this.get(`categories/${id}`);
  }
}
