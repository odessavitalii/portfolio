import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

import {Bill} from '../models/bill.model';
import {BaseApi} from '../../../shared/services/base-api.service';

@Injectable()
export class BillService extends BaseApi {

  constructor(public http: HttpClient) {
    super(http);
  }

  getBill(): Observable<Bill> {
    return this.get('bill');
  }

  updateBill(bill: Bill): Observable<Bill> {
    return  this.put('bill', bill);
  }

  getCurrency(): Observable<any> {
    return this.http.get(`https://www.cbr-xml-daily.ru/daily_json.js`);
  }
}
