import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

import {UsersService} from '../../shared/services/users.service';
import {User} from '../../shared/models/user.model';
import {Meta, Title} from '@angular/platform-browser';

@Component({
  selector: 'wfm-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private usersService: UsersService,
    private router: Router,
    private title: Title,
    private meta: Meta
  ) {
    title.setTitle('Регистрация в системе');
    meta.addTags([
      {name: 'keywords', content: 'регистрация, система'},
      {name: 'descriptions', content: 'Страница для регистрации в системе'}
    ]);
  }

  ngOnInit() {

    this.form = this.fb.group({
      email: ['', [Validators.email, Validators.required], this.forbiddenEmails.bind(this)],
      password: ['', [Validators.minLength(6), Validators.required]],
      name: ['', [Validators.required]],
      agree: [false, [Validators.requiredTrue, Validators.required]],
    });

  }

  onSubmit() {
    const {email, password, name} = this.form.value;
    const user = new User(email, password, name);

    this.usersService.createUser(user).subscribe(() => {
     this.router.navigate(['/login'], {
       queryParams: {
         nowCanLogin: true
       }
     }).then();
    });
  }

  forbiddenEmails(control: FormControl): Promise<any> {
    return new Promise((resolve, reject) => {
      this.usersService.getUserByEmail(control.value).subscribe((user: User) => {
        if (user) {
          resolve({
            forbiddenEmail: true
          });
        } else {
          resolve(null);
        }
      });
    });
  }

}
