import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {AUTH_ROUTES} from './auth-routing.module';
import {SharedModule} from '../shared/shared.module';

import {AuthComponent} from './auth.component';
import {LoginComponent} from './login/login.component';
import {RegistrationComponent} from './registration/registration.component';

@NgModule({
  declarations: [
    AuthComponent,
    LoginComponent,
    RegistrationComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(AUTH_ROUTES),
    SharedModule
  ],
  exports: [],
  providers: []
})

export class AuthModule { }
