import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {LoaderComponent} from './components/loader/loader.component';

@NgModule({
  declarations: [LoaderComponent],
  imports: [
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    LoaderComponent
  ],
  providers: []
})

export class SharedModule { }
