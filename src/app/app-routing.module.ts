import {Routes} from '@angular/router';

import {NotFoundComponent} from './shared/components/not-found/not-found.component';

export const APP_ROUTES: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'system', loadChildren: './system/system.module#SystemModule'},
  {path: '**', component: NotFoundComponent}
];
